var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');
var app = express();

var request = require('request');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('views', path.join(__dirname, 'public', 'view'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));

// RUTAS Y REST
var usuario = require('./app/routes/usuario');

app.use('/usuario', usuario);
app.get('/', function(req, res){
  request.get("http://localhost:4500/usuario", (error, response, body) => {
    if(error) return console.dir(error);
    // console.dir(JSON.parse(body));
    res.render('index', { usuarios: JSON.parse(body) } );
    // res.json(JSON.parse(body));
  });
});

app.listen(4500);
