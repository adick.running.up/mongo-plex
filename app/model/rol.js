var conexion = require('./db');

var schema = conexion.Schema;

var rolSchema = new schema({
  nombre: String,
  descripcion: String,
  estado: Number,
  fecha_creacion: Date,
  fecha_modificacion: Date,
  usuario_creacion: ObjectId,
  usuario_modificacion: ObjectId,
});

var Rol = conexion.model('col_rol', rolSchema);
module.exports = Rol;
