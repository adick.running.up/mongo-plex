const mongoose = require('mongoose');

// CONFIGURACION PARA ENTORNO LOCALHOST
mongoose.connect('mongodb://localhost/mongoplex');

module.exports = mongoose;
