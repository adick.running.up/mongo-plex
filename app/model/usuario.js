var conexion = require('./db');

var schema = conexion.Schema;

var usuarioSchema = new schema({
  nombre: String,
  correo: String,
  password: String,
  fecha_creacion: Date,
  fecha_modificacion: Date,
  observacion: String,
  estado: Number
});
// usuario_creacion: ObjectId,
// usuario_modificacion: ObjectId,

var Usuario = conexion.model('col_usuario', usuarioSchema);
module.exports = Usuario;
