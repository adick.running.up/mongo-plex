var express = require('express');
var router = express.Router();
var usuarioCOL = require('../model/usuario');

router.route('/:id')
  .get(function(req, res){
    usuarioCOL.findById(req.params.id,
      function (err, usuario) {
        if (err) return handleError(err);
        res.json(usuario)
      }
    );
  })
  .put(function(req, res){
    // console.log(req);
    usuarioCOL.findById(req.params.id,
      function (err, usuario) {
        if (err) return handleError(err);
        // console.log(req.body[0]);
        usuario.nombre = req.body[0].nombre;
        usuario.correo = req.body[0].correo;
        // usuario.password = req.body.password;
        // usuario.fecha_creacion = new Date();
        usuario.fecha_modificacion = new Date();
        usuario.observacion = req.body[0].observacion;
        usuario.estado = req.body[0].estado;

        usuario.save(function(err) {
          if (err) return handleError(err);
          res.json({ message: '¡Cambios guardados!' });
        });
      }
    );
  })
  .delete(function(req, res) {
    usuarioCOL.remove({ _id: req.params.id },
      function(err, usuario) {
        if (err) return handleError(err);
        res.json({ message: '¡Usuario Eliminado!' });
      }
    );
  });

router.route('/')
  .get(function(req, res){
    usuarioCOL.find({},
      function (err, usuario) {
        if (err) return handleError(err);
        res.json(usuario)
      }
    );
  })
  .post(function(req, res){
    var nuevoUsuario = new usuarioCOL();
    nuevoUsuario.nombre = req.body[0].nombre;
    nuevoUsuario.correo = req.body[0].correo;
    // nuevoUsuario.password = req.body.password;
    nuevoUsuario.fecha_creacion = new Date();
    nuevoUsuario.fecha_modificacion = new Date();
    nuevoUsuario.observacion = req.body[0].observacion;
    nuevoUsuario.estado = req.body[0].estado;
    nuevoUsuario.save(
      function (err) {
        if (err) return handleError(err);
        res.send(JSON.stringify(nuevoUsuario))
      }
    );
  });
module.exports = router;
